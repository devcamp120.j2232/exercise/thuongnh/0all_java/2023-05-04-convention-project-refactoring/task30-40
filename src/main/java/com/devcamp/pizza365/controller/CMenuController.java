package com.devcamp.pizza365.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.pizza365.model.CMenu;
import com.devcamp.pizza365.responsitory.IMenuResponsitory;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@CrossOrigin
@RestController
@RequestMapping("/cmenu")
public class CMenuController {
    // CRUD cho CMenu

    @Autowired
    IMenuResponsitory pIMenuResponsitory;

    @GetMapping("/all")
    public ResponseEntity<List<CMenu>> getMenul() {
        try {
            List<CMenu> vMenu = new ArrayList<CMenu>(); // khởi tạo list chứa đối tượng
            pIMenuResponsitory.findAll().forEach(vMenu::add);
            return new ResponseEntity<>(vMenu, HttpStatus.OK);

        } catch (Exception e) {
            // TODO: handle exception
            System.out.println("Exception mới nhất ");
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // create
    // //{
    // "size": "DUONG",
    // "duongKinh": "30",
    // "suongNuong": 2,
    // "salad": 7,
    // "nuocNgot": 4,
    // "donGia": 20 }
    

    @PostMapping("/create")
    public ResponseEntity<CMenu> getCreate(@RequestBody CMenu paramCmenu) {
        try {
            CMenu _menu = pIMenuResponsitory.save(paramCmenu);

            return new ResponseEntity<>(_menu, HttpStatus.CREATED);

        } catch (Exception e) {
            // TODO: handle exception
            System.out.println("Exception mới nhất ");
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // update
    @PutMapping("/update/{Cmenu}")
    public ResponseEntity<Object> getUpdate(@PathVariable("Cmenu") int id, @RequestBody CMenu pMenu) {

        Optional<CMenu> _menu = pIMenuResponsitory.findById(id);
        // kiểm tra có null hay k ,, true là khác null
        if (_menu.isPresent()) {
            // get(); là của Optional để lấy giá tri của đối tượng
            CMenu _menuNew = _menu.get(); // lấy giá trị của _menu gán cho _user
            _menuNew.setSize(pMenu.getSize());
            _menuNew.setDonGia(pMenu.getDonGia());
            _menuNew.setDuongKinh(pMenu.getDuongKinh());
            _menuNew.setNuocNgot(pMenu.getNuocNgot());
            _menuNew.setSalad(pMenu.getSalad());

            try {
                return ResponseEntity.ok(pIMenuResponsitory.save(_menuNew));

            } catch (Exception e) {
                // TODO: handle exception
                return ResponseEntity.unprocessableEntity()
                        .body("can not execute operation of this Entity" + e.getCause().getCause().getMessage());
                // không thể thực thi hoạt động của Thực thể này
            }
        } else {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }
    // delete 
    @DeleteMapping("/delete/{Cmenu}")
    public ResponseEntity<Object> getDelete(@PathVariable int Cmenu ){
       Optional< CMenu> _cmenu = pIMenuResponsitory.findById(Cmenu);
        //nếu kết quả khác null 
        if(_cmenu != null){
            try {
                CMenu vMenu = _cmenu.get();
                pIMenuResponsitory.delete(vMenu);
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
                
            } catch (Exception e) {
                // TODO: handle exception
                System.out.println(e);
                return new ResponseEntity<>(null ,HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
        else {
            // báo là không tìm thấy 
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }

    }


}
