package com.devcamp.pizza365.controller;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.pizza365.model.CCountry;
import com.devcamp.pizza365.model.CDrink;
import com.devcamp.pizza365.responsitory.IDrinkResponsitory;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;


@CrossOrigin
@RestController
@RequestMapping("/drink")
public class CDrinkController {

    // CRUD cho drink
    @Autowired
    IDrinkResponsitory pIDrinkResponsitory;
    @GetMapping("/all")
    public ResponseEntity<List<CDrink>> getAllDrink() {
        try {
            List<CDrink> vDrink = new ArrayList<CDrink>();  // khởi tạo list chứa đối tượng
            pIDrinkResponsitory.findAll().forEach(vDrink::add);
            return new ResponseEntity<>(vDrink, HttpStatus.OK);

        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

     // create  mẫu json thử 
    //  {
    //     "maNuocUong": "DUONG",
    //     "tenNuocUong": "30",
    //     "donGia": 2
    // }
     @PostMapping("/create")
     public ResponseEntity<CDrink> getCreate(@RequestBody CDrink paramCDrink) {
         try {
            paramCDrink.setNgayTao(new Date().getTime());  // set ngay tao 
             CDrink _menu = pIDrinkResponsitory.save(paramCDrink);
             return new ResponseEntity<>(_menu, HttpStatus.CREATED);
         } catch (Exception e) {
             // TODO: handle exception
             System.out.println("Exception mới nhất ");
             System.out.println(e);
             return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
         }
     }
 
     // update
     @PutMapping("/update/{idDrink}")
     public ResponseEntity<Object> getUpdate(@PathVariable("idDrink") long id, @RequestBody CDrink pDrink) {
 
         Optional<CDrink> _drink = pIDrinkResponsitory.findById(id);
         // kiểm tra có null hay k ,, true là khác null
         if (_drink.isPresent()) {
            Date date = new Date();
             // get(); là của Optional để lấy giá tri của đối tượng
             CDrink _drinkNew = _drink.get(); // lấy giá trị của _menu gán cho _user
             _drinkNew.setDonGia(pDrink.getDonGia());
             _drinkNew.setMaNuocUong(pDrink.getMaNuocUong());
             _drinkNew.setTenNuocUong(pDrink.getTenNuocUong());
             _drinkNew.setNgayCapNhat(date.getTime());  // nhạn tham số là kiểu long . 
             try {
                 return ResponseEntity.ok(pIDrinkResponsitory.save(_drinkNew));  // lưu vào data base 
             } catch (Exception e) {
                 // TODO: handle exception
                 return ResponseEntity.unprocessableEntity()
                         .body("can not execute operation of this Entity" + e.getCause().getCause().getMessage());
                 // không thể thực thi hoạt động của Thực thể này
             }
         } else {
             return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
 
         }
     }
     // delete 
     @DeleteMapping("/delete/{idDrink}")
     public ResponseEntity<Object> getDelete(@PathVariable long idDrink ){
        Optional< CDrink> _drink = pIDrinkResponsitory.findById(idDrink);
         //nếu kết quả khác null 
         if(_drink != null){
             try {
                CDrink vDrink = _drink.get();
                 pIDrinkResponsitory.delete(vDrink);   // delete trên data base 
                 return new ResponseEntity<>(HttpStatus.NO_CONTENT);
                 
             } catch (Exception e) {
                 // TODO: handle exception
                 System.out.println(e);
                 return new ResponseEntity<>(null ,HttpStatus.INTERNAL_SERVER_ERROR);
             }
         }
         else {
             // báo là không tìm thấy 
             return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
         }
 
     }

    

    
}
