package com.devcamp.pizza365.model;

import java.util.Date;
import java.util.Set;  
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import com.fasterxml.jackson.annotation.JsonManagedReference;




@Entity   // Entity  tự động tạo câu lệnh sql
@Table(name = "user")   // đặt tên bảng dữ liệu
public class CUser {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id; 
    
    @Column(name = "fullName")
    private String fullName;

    @Column(name = "phone")
    private String phone;

    @Column(name = "address")
    private String address;

    @Temporal(TemporalType.TIMESTAMP)
    @CreatedDate     ///ngày được tạo ở đây 
    @Column(name = "ngay_tao", nullable = true, updatable = false)  // không cho update  updatable = false
    private Date ngayTao;

    @Temporal(TemporalType.TIMESTAMP)
    @LastModifiedDate   // trả về ngày sửa đổi cuối tùng đúng không
    @Column(name = "ngay_cap_nhap",nullable = true)  // trường cơ sở dũ liệu này cos thể null 
    private Date ngayCapNhap;


    @OneToMany(mappedBy = "user", cascade =  CascadeType.ALL)
    
    private Set<COrder>  order;


    public CUser() {
        super();   //  Java sẽ tự động thêm lệnh gọi đến constructor mặc định của lớp cha Object.
    }


    public CUser(long id, String fullName, String phone, String address, Date ngayTao, Date ngayCapNhap,
            Set<COrder> order) {
        this.id = id;
        this.fullName = fullName;
        this.phone = phone;
        this.address = address;
        this.ngayTao = ngayTao;
        this.ngayCapNhap = ngayCapNhap;
        this.order = order;
    }

    

    // không có id 
    public CUser(String fullName, String phone, String address) {
        super();
        this.fullName = fullName;
        this.phone = phone;
        this.address = address;
    }


    public long getId() {
        return id;
    }


    public void setId(long id) {
        this.id = id;
    }


    public String getFullName() {
        return fullName;
    }


    public void setFullName(String fullName) {
        this.fullName = fullName;
    }


    public String getPhone() {
        return phone;
    }


    public void setPhone(String phone) {
        this.phone = phone;
    }


    public String getAddress() {
        return address;
    }


    public void setAddress(String address) {
        this.address = address;
    }


    public Date getNgayTao() {
        return ngayTao;
    }


    public void setNgayTao(Date ngayTao) {
        this.ngayTao = ngayTao;
    }


    public Date getNgayCapNhap() {
        return ngayCapNhap;
    }


    public void setNgayCapNhap(Date ngayCapNhap) {
        this.ngayCapNhap = ngayCapNhap;
    }


    public Set<COrder> getOrder() {
        return order;
    }


    public void setOrder(Set<COrder> order) {
        this.order = order;
    }

    

    

    


    
    

}
